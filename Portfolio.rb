require 'date'

class Portfolio
    
    def initialize(stocks)
        @stocks = stocks
    end

    def profit(init_date, end_date)
        profit = @stocks.reduce(0) { |sum, stock| sum + stock.Price(end_date) - stock.Price(init_date) }
        cumulative_return = profit.fdiv(@stocks.reduce(0) { |sum, stock| sum + stock.Price(init_date) })
        annualized_return = (1 + cumulative_return)**((365).fdiv((end_date-init_date).to_i)) - 1
        return profit, annualized_return
    end
end

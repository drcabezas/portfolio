# Portfolio

Hola :3, en este repositorio encontrarás mi respuesta a la tarea de la primera etapa de postulación a Fintual.


## Tareita

Construct a simple Portfolio class that has a collection of Stocks and a "Profit" method that receives 2 dates and returns the profit of the Portfolio between those dates. Assume each Stock has a "Price" method that receives a date and returns its price.
Bonus Track: make the Profit method return the "annualized return" of the portfolio between the given dates.


